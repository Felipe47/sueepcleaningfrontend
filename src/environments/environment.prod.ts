export const environment = {
  production: true,
  // serverUrl: 'https://office.sueep.com/api/method/cleaning_services.cleaning_services.doctype.cleaning_quote.cleaning_quote.',
  serverUrl: 'https://dev-office.sueep.com/api/method/cleaning_services.cleaning_services.doctype.cleaning_quote.cleaning_quote.',
};
