import { NgbModule, NgbTimepickerModule, NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule, MatButtonModule, MatFormFieldModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FullComponent } from './layouts/full/full.component';
import { HomeModule } from './home/home.module';
import { ContactModule } from './contact/contact.module';
import { ServiceModule } from './service/service.module';
import { ServicePageComponent } from './service/service-page/service-page.component';
import { ContactPageComponent } from './contact/contact-page/contact-page.component';
import { AboutPageComponent } from './home/about-page/about-page.component';
import { MaterialModule } from './material.module';
import { BookingModule } from './booking/booking.module';
import { BookingPageComponent } from './booking/booking-page/booking-page.component';
import { DownloadModule } from './download/download.module';
import { DownloadPageComponent } from './download/download-page/download-page.component';
import * as $ from 'jquery';
import { AirbnbBookingComponent } from './service/airbnb-booking/airbnb-booking.component';
import { CommercialBookingComponent } from './service/commercial-booking/commercial-booking.component';
import { BookingService } from './shared/auth/booking.service';
import { HttpClientModule, HttpClientXsrfModule } from '@angular/common/http';
import { Ng2FlatpickrModule } from 'ng2-flatpickr';
import { AgmCoreModule } from '@agm/core';
import { SharedModule } from './shared/shared.module';
import { InitialQuoteService } from './shared/auth/initial-quote.service';
import { FAQModule } from './faq/faq.module';


@NgModule({
  declarations: [
    AppComponent,
    FullComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientXsrfModule,
    // Ng2FlatpickrModule,
    SharedModule,
    AgmCoreModule.forRoot({apiKey: 'AIzaSyAeH_Qxdbc74ahPn-cKbiwI1oJj48E7ZG0'}),
    HomeModule,
    ContactModule,
    ServiceModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    BookingModule,
    BrowserAnimationsModule,
    NgbTimepickerModule,
    NgbDatepickerModule,
    DownloadModule,
    FAQModule,
  ],
  providers: [BookingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
