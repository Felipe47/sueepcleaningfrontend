import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookingPageComponent } from './booking-page/booking-page.component';
import { ContractComponent } from './contract/contract.component';


const routes: Routes = [
  {
    path: '',
    component: BookingPageComponent
  },
  {
    path: 'contract',
    component: ContractComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BookingRoutingModule { }
