import { ServicePageComponent } from './../../service/service-page/service-page.component';
import { BookingInfo } from './../../service/booking-info';
import { Component, OnInit, Input, ViewChild, ElementRef, NgZone } from '@angular/core';
import flatpickr from 'flatpickr';
import { MapsAPILoader, AgmMap } from '@agm/core';
import { NgForm, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { BookingService } from 'src/app/shared/auth/booking.service';
import { FlatpickrOptions } from 'ng2-flatpickr';
import { Router, Params, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { AvailableTimeModel } from 'src/app/models/available-time';
import { PromoCodeModel } from 'src/app/models/promo-code';
import { NgbDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { stringify } from '@angular/compiler/src/util';
import { analyzeAndValidateNgModules } from '@angular/compiler';


declare var require: any;
const data: any = require('../../shared/data/booking.json');

@Component({
  selector: 'app-booking-page',
  templateUrl: './booking-page.component.html',
  styleUrls: ['./booking-page.component.scss']
})

export class BookingPageComponent implements OnInit {
  first_name: string;
  last_name: string;
  email: string;
  phone: string;
  bathroom_number: number;
  bedroom_number: number;
  zip_code: string;
  city: string;
  dateVar: Date;
  state: string;
  message: any;
  dateString: string;
  extras: Array<String> = ['Inside Cabinets', 'Inside Fridge', 'Inside Oven', 'Laundry WashDry', 'Interior Windows'];
  public latitude: number;
  public longitude: number;
  public zoom: number;
  public latlongs: any = [];
  public latlong: any = {};
  public searchControl: FormControl;
  address: any;
  form: any = {};
  cleaning_type: string;
  extra_amount: number;
  price: number = 0;
  subtotal_amount: number;
  discount_amount: number = 0;
  card_number: any;
  creditCardInfo: any;
  selectedExtraValues: any = {};
  now: Date = new Date();
  minDate: any;
  maxDate: any;
  hours: any[] = [];
  public errorMessage: any;
  public loading: boolean;
  public hourLoading: boolean;
  public exampleOptions: FlatpickrOptions = {
    defaultDate: new Date(),
    minDate: 'today',
    dateFormat: 'Y-m-d',
    onChange: (selectedDates, dateStr, instance) => {
      this.dateString = dateStr;
    },
    wrap: true,
    allowInput: false
  };
  geocoder: any;
  available_time_info: AvailableTimeModel;
  promo_code_info: PromoCodeModel;

  @ViewChild('search', { static: false })
  public searchElementRef: ElementRef;
  @ViewChild('f', { static: false }) residentialBookingForm: NgForm;
  @ViewChild(AgmMap, { static: true }) map: AgmMap;


  constructor(private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone, private bookingService: BookingService, private route: ActivatedRoute, private router: Router) {
    this.loading = false;
    this.hourLoading = false;
    this.mapsAPILoader = mapsAPILoader;
    this.mapsAPILoader.load().then(() => {
      this.geocoder = new google.maps.Geocoder();
    });
  }

  ngOnInit() {
    this.form.checkbox1 = false;
    this.form.checkbox2 = false;
    this.form.checkbox3 = false;
    this.form.checkbox4 = false;
    this.form.checkbox5 = false;
    this.form.cleaning_type = 'Standard Cleaning';
    this.form.recurrency = '';
    this.form.agreement = true;
    this.zoom = 3;
    this.latitude = 39.953021;
    this.longitude = -75.163479;
    this.searchControl = new FormControl();
    const dateVar = new Date(new Date().getTime() + 86400000);
    this.minDate = { year: dateVar.getFullYear(), month: dateVar.getMonth() + 1, day: dateVar.getDate() };
    this.form.date = this.minDate;
    this.route.queryParams.subscribe(params => {
      const customer_info = params;
      this.first_name = customer_info.first_name;
      this.last_name = customer_info.last_name;
      this.bedroom_number = customer_info.bedroomNumber * 1;
      this.bathroom_number = customer_info.bathroomNumber * 1;
      this.phone = customer_info.phone;
      this.email = customer_info.email;
      this.zip_code = customer_info.zip_code;
      this.form.city = customer_info.city;
      this.form.state = customer_info.state;
      this.form.additional_information = customer_info.additional_information;
      this.searchControl.setValue(customer_info.address);
      this.address = customer_info.address;
      this.form.apartment_number = customer_info.apartment_number;
      this.form.number1 = customer_info.card_number;
      this.form.card_holder = customer_info.card_first_name;
      this.form.card_holder_lastname = customer_info.card_last_name;
      this.form.month = customer_info.card_exp_month;
      this.form.year = customer_info.card_exp_year;
      if (customer_info.extraService) {
        for (let i = 0; i < customer_info.extraService.length; i++) {
          this.selectedExtraValues[customer_info.extraService[i]] = true;
        }
      }

    });
    $.getScript('./assets/js/card.js');
    this.onSelect(this.minDate);
    this.calculatePayment();
    this.calculateExtrasPayment();
    this.getPlaces();
  }

  getPlaces() {
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: [],
        componentRestrictions: { 'country': 'USA' }
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
          const latlong = {
            latitude: place.geometry.location.lat(),
            longitude: place.geometry.location.lng()
          };
          this.latlongs.push(latlong);
          this.address = place.name + '' + place.formatted_address;
        });
      });
    });
  }

  getSelectedExtrasValue(service: string) {
    if (Boolean(this.selectedExtraValues[service])) {
      delete this.selectedExtraValues[service];
    } else {
      this.selectedExtraValues[service] = true;
    }
    this.calculateExtrasPayment();
  }

  calculateExtrasPayment() {
    this.extra_amount = Object.keys(this.selectedExtraValues).length * 15;
    this.calculateTotal();
  }
  calculateTotal() {
    this.subtotal_amount = this.price + this.extra_amount + this.discount_amount;
  }

  calculatePayment() {
    if (this.form.cleaning_type === 'Standard Cleaning') {
      this.price = data.standard_cleaning[this.bedroom_number - 1][this.bathroom_number - 1];
    } else
      if (this.form.cleaning_type === 'Deep Cleaning') {
        this.price = data.deep_cleaning[this.bedroom_number - 1][this.bathroom_number - 1];
      } else
        if (this.form.cleaning_type === 'Move in-out Cleaning') {
          this.price = data.move_cleaning[this.bedroom_number - 1][this.bathroom_number - 1];
        } else
          if (this.form.cleaning_type === 'Post Construction Cleaning') {
            this.price = data.postconstruction_cleaning[this.bedroom_number - 1][this.bathroom_number - 1];
          } else {
            this.price = 0;
          }
    this.calculateTotal();
  }

  onSelect(event: any) {
    this.hourLoading = true;
    const hours = {
      id: 3, 'name': 'felipe',
    };
    this.dateString = event.year + '-' + event.month + '-' + event.day;
    this.available_time_info = new AvailableTimeModel(
      this.form.cleaning_type,
      this.email,
      this.form.city,
      this.form.state,
      this.dateString,
      this.bathroom_number,
      this.bedroom_number,
    );
    this.bookingService.check_available_time(this.available_time_info).subscribe(
      data => {
        this.hours = data.message.hours;
        this.form.available_time = this.hours[0];
        this.hourLoading = false;
      },
      error => {
        this.errorMessage = error;
        this.hourLoading = false;
        Swal.fire({
          type: 'error',
          title: 'Error...',
          text: 'We could not load available time for this city'
        });
        // this.router.navigate(['/']);
      }
    );
  }

  checkPromoCode() {
    this.promo_code_info = new PromoCodeModel(
      this.email,
      this.form.promo_code
    );
    this.bookingService.check_promo_code(this.promo_code_info).subscribe(
      data => {
        // this.dato = JSON.parse(data.data);
      },
    );
  }


  sendQuote() {
    this.loading = true;
    this.calculatePayment();
    this.calculateExtrasPayment();
    const url = 'api/user/create';
    this.creditCardInfo = {
      'first_name': this.form.card_holder,
      'lastname': this.form.card_holder_lastname,
      'card_number': this.form.number1.split(' - ').join(''),
      'exp_date': this.form.month + '/' + this.form.year,
      'CCV': (this.form.CCV).toString()
    };
    const bookingInfoModel = {
      quote_type: 'Residential',
      cleaning_type: this.form.cleaning_type,
      date: this.dateString,
      available_time: this.form.available_time,
      first_name: this.first_name,
      last_name: this.last_name,
      email: this.email,
      phone: this.phone,
      bedroom_number: this.bedroom_number,
      bathroom_number: this.bathroom_number,
      company_name: '',
      building_type: '',
      number_rental: '',
      average_cleaners: '',
      additional_information: this.form.additional_information,
      address: this.address,
      apartment_number: this.form.apartment_number,
      zip_code: this.zip_code,
      city: this.form.city,
      state: this.form.state,
      extras: JSON.stringify(this.selectedExtraValues),
      cardInfo: JSON.stringify(this.creditCardInfo),
      promo_code: this.form.promo_code,
      total_amount: this.subtotal_amount,
      recurrency: this.form.recurrency
    };
    this.bookingService.create_quote(bookingInfoModel).subscribe(
      data => {
        this.loading = false;
        // this.dato = JSON.parse(data.data);
        if (data.message[1] === '200') { // Transaction declined by the bank
          Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Sorry, the payment was declined by the bank.',
            footer: 'if you think this is a mistake, chat with our support team, thanks!'
          });
        } else if (data.message[1] === '6') { // Wrong card number
          Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Sorry, payment could not be completed because the credit card number',
            footer: 'If you think this is a mistake, chat with our support team, thank you!'
          });
        } else if (data.message[1] !== '1') { // Error processing the payment
          Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Sorry, payment could not be completed, please chat with our support team for more information, thank you '
          });
        } else {
          Swal.fire(
            'Thanks for trusting us!',
            'Your request has been succesfully scheduled!',
            'success'
          );
          this.router.navigate(['']);
        }
      },
      error => {
        this.errorMessage = error;
        this.loading = false;
        Swal.fire({
          type: 'error',
          title: 'Error...',
          text: 'Check your booking form please, if you think this is a mistake, chat with our support team , sorry for the inconvinients'
        });
      }
    );
  }

  /*Map functions*/
  setCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 8;
      });
    }
  }

}







