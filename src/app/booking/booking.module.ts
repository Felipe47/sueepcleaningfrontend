import { MaterialModule } from './../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookingRoutingModule } from './booking-routing.module';
import { BookingPageComponent } from './booking-page/booking-page.component';
import { SharedModule } from '../shared/shared.module';
import { Ng2FlatpickrModule } from 'ng2-flatpickr';
import { AgmCoreModule } from '@agm/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ContractComponent } from './contract/contract.component';

@NgModule({
  declarations: [BookingPageComponent, ContractComponent],
  imports: [
    CommonModule,
    BookingRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2FlatpickrModule,
    NgbModule,
    MaterialModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAeH_Qxdbc74ahPn-cKbiwI1oJj48E7ZG0',
      libraries: ['places']
    })
  ]
})
export class BookingModule { }
