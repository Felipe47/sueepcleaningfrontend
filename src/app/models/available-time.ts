// available-time model
export class AvailableTimeModel {
  cleaning_type: string;
  email: string;
  city: string;
  state: string;
  date: string;
  bathrooms: number;
  bedrooms: number;

  constructor(cleaning_type: string, email: string, city: string, state: string,
    date: string, bathrooms: number, bedrooms: number) {
    this.email = email;
    this.city = city;
    this.state = state;
    this.date = date;
    this.cleaning_type = cleaning_type;
    this.bathrooms = bathrooms;
    this.bedrooms = bedrooms;
  }
}
