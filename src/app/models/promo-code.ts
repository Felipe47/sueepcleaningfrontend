// promo-code model
export class PromoCodeModel {
  email: string;
  promo_code: string;

  constructor(email: string,
    promo_code: string) {
    this.email = email;
    this.promo_code = promo_code;
  }
}
