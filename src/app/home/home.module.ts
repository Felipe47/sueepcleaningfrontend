import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { WelcomePageComponent } from './welcome-page/welcome-page.component';
import { AboutPageComponent } from './about-page/about-page.component';
import { ServiceModule } from '../service/service.module';
import { ContactModule } from '../contact/contact.module';
import { NavbarComponent } from '../shared/navbar/navbar.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [WelcomePageComponent, AboutPageComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    ServiceModule,
    ContactModule,
    SharedModule
  ],
  exports: [
    AboutPageComponent,
    WelcomePageComponent
  ]
})
export class HomeModule { }
