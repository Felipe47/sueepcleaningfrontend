import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DownloadRoutingModule } from './download-routing.module';
import { DownloadPageComponent } from './download-page/download-page.component';


@NgModule({
  exports: [
    DownloadPageComponent
  ],
  declarations: [DownloadPageComponent],
  imports: [
    CommonModule,
    DownloadRoutingModule
  ]
})
export class DownloadModule { }
