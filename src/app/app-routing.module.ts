import { HomeModule } from './home/home.module';
import { AboutPageComponent } from './home/about-page/about-page.component';
import { FullComponent } from './layouts/full/full.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Full_ROUTES } from './shared/routes/full-layout.routes';
import { WelcomePageComponent } from './home/welcome-page/welcome-page.component';
import { ServicePageComponent } from './service/service-page/service-page.component';
import { ContactPageComponent } from './contact/contact-page/contact-page.component';
import { BookingPageComponent } from './booking/booking-page/booking-page.component';
import { DownloadPageComponent } from './download/download-page/download-page.component';
import { AirbnbBookingComponent } from './service/airbnb-booking/airbnb-booking.component';
import { CommercialBookingComponent } from './service/commercial-booking/commercial-booking.component';

// Instanciamos una variable que me contenga las rutas por las cuales la aplicación se va a mover.
const appRoutes: Routes = [
  // {
  //   path: '',
  //   redirectTo: 'home',
  //   pathMatch: 'full',
  // },
  // Definimos un arreglo de rutas "Full_ROUTES" para modularizar la aplicación y hacerla más mantenible.
  { path: '', component: FullComponent, children: Full_ROUTES}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
