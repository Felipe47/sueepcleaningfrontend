
// booking info model
export class BookingInfo {
  first_name: string;
  last_name: string;
  email: string;
  phone: string;
  bedroomNumber: number;
  bathroomNumber: number;
  zip_code: number;
  city: string;
  state: string;

  constructor(first_name: string, last_name: string, email: string,
    phone: string, bedroomNumber: number, bathroomNumber: number,
    zip_code: number, city: string, state: string) {
    this.first_name = first_name;
    this.last_name = last_name;
    this.email = email;
    this.phone = phone;
    this.bedroomNumber = bedroomNumber;
    this.bathroomNumber = bathroomNumber;
    this.zip_code = zip_code;
    this.city = city;
    this.state = state;
  }
}
