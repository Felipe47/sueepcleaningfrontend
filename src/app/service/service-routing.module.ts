import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServicePageComponent } from './service-page/service-page.component';
import { CommercialBookingComponent } from './commercial-booking/commercial-booking.component';
import { AirbnbBookingComponent } from './airbnb-booking/airbnb-booking.component';

const routes: Routes = [
  {
    path: 'commercial',
    component: CommercialBookingComponent
  },
  {
    path: 'airbnb',
    component: AirbnbBookingComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiceRoutingModule { }
