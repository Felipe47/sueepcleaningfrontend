import { ServicePageComponent } from './service-page/service-page.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServiceRoutingModule } from './service-routing.module';
import { CommercialBookingComponent } from './commercial-booking/commercial-booking.component';
import { AirbnbBookingComponent } from './airbnb-booking/airbnb-booking.component';
import { AgmCoreModule } from '@agm/core';
import { Ng2FlatpickrModule } from 'ng2-flatpickr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  exports: [
    CommonModule,
    NgbModule,
    CommercialBookingComponent,
    AirbnbBookingComponent,
    ServicePageComponent
  ],
  imports: [
    CommonModule,
    ServiceRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2FlatpickrModule,
    NgbModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAeH_Qxdbc74ahPn-cKbiwI1oJj48E7ZG0',
      libraries: ['places']
    })
  ],
  declarations: [CommercialBookingComponent, AirbnbBookingComponent, ServicePageComponent]
})
export class ServiceModule { }
