import { InitialQuoteService } from './../../shared/auth/initial-quote.service';
import { FullComponent } from './../../layouts/full/full.component';
import { BookingInfo } from './../booking-info';
import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { Router, Params } from '@angular/router';
import { BookingService } from 'src/app/shared/auth/booking.service';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BookingPageComponent } from 'src/app/booking/booking-page/booking-page.component';
import { FlatpickrOptions } from 'ng2-flatpickr';
import { BehaviorSubject } from 'rxjs';
import Swal from 'sweetalert2';

declare var require: any;
const available_time_data: any = require('../../shared/data/zip_codes.json');


@Component({
  selector: 'app-service-page',
  templateUrl: './service-page.component.html',
  styleUrls: ['./service-page.component.scss']
})
export class ServicePageComponent implements OnInit {
  private messageSource = new BehaviorSubject<any>('default message');
  errorMessage: string;
  name: string;
  email: string;
  phone: string;
  bedroom_num: string;
  bathroom_num: string;
  city: string;
  state: string;
  dato: any;
  customer_info: BookingInfo;
  form: any = {};
  sum = 0;
  sumFlag = false;
  buttonDisabled: boolean;
  validateBooking: boolean = false;

  @ViewChild('f', { static: false }) servicesForm: NgForm;

  constructor(private router: Router,
    private initialQuoteService: InitialQuoteService,
    private serviceForm: FormBuilder) { }

  ngOnInit() {
    this.form.bathroomNumber = '1';
    this.form.bedroomNumber = '1';
  }

  checkBathBedNumber() {
    this.sum = (Number(this.form.bedroomNumber * 1) + (Number(this.form.bathroomNumber * 1)));
    if (this.sum > 0) {
      this.sumFlag = true;
    } else {
      this.sumFlag = false;
    }
  }

  private checkZipCode(zip_code: any) {
    for (let i = 0; i < available_time_data.length; i++) {
      if (available_time_data[i].zip_codes.includes(zip_code)) {
        this.city = available_time_data[i].city;
        this.state = available_time_data[i].state;
        return true;
      }
    }
  }
  // checkZipCode(zip_code: any) {
  //   const zip_code_model = {
  //     'zip_code': zip_code
  //   }
  //   this.initialQuoteService.check_zone(zip_code_model).subscribe(
  //     data => {
  //       alert('data: ' + JSON.stringify(data));
  //       if (data.message.status === 'Ok') {
  //         this.city = data.message.city;
  //         this.state = data.message.state;
  //         this.validateBooking = true;
  //       }
       
  //     }
  //   );
  // }

  redirectToBooking() {
    this.checkBathBedNumber();
    const validateBooking = this.checkZipCode(this.form.zip_code);
    if (this.sumFlag === true) {
      if (validateBooking === true) {
        this.customer_info = new BookingInfo(
          this.form.first_name,
          this.form.last_name,
          this.form.email,
          this.form.phone,
          this.form.bedroomNumber,
          this.form.bathroomNumber,
          this.form.zip_code,
          this.city,
          this.state
        );
        // alert(JSON.stringify(this.customer_info));
        this.send_initial_quote(this.customer_info);
        if (this.send_initial_quote) {
          this.router.navigate(['/cleaningservices'], { queryParams: this.customer_info });
        }
      } else {
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'We are not currently in your area :(',
          footer: '<a href>Why do I have this issue?</a>'
        });
      }
    } else {
      Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: 'You have to pick at least 1 bathroom or 1 bedroom',
      });
    }
  }

  send_initial_quote(customer_info: any) {
    this.initialQuoteService.initial_quote(customer_info).subscribe(
      data => {
      },
    );
  }
}
