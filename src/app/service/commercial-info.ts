// commercial info model
export class ServiceBookingModel {
  quote_type: string;
  cleaning_type: string;
  first_name: string;
  last_name: string;
  email: string;
  phone: string;
  bedroom_number: number;
  bathroom_number: number;
  date: string;
  available_time: string;
  company_name: string;
  building_type: string;
  number_rental: string;
  average_cleaners: number;
  message: string;
  location: string;
  extras: [];
  card_number: string;
  card_holder: string;
  exp_date: string;
  CCV: string;
  total_amount: number;
  constructor(quote_type: string,
    cleaning_type: string,
    first_name: string,
    last_name: string,
    date: string,
    available_time: string,
    email: string,
    phone: string,
    bedroom_number: number,
    bathroom_number: number,
    company_name: string,
    building_type: string,
    number_rental: string,
    average_cleaners: number,
    message: string,
    extras: [],
    total_amount: number,
    location: string,
    card_number: string,
    card_holder: string,
    exp_date: string,
    CCV: string) {
    this.quote_type = quote_type;
    this.cleaning_type = cleaning_type;
    this.first_name = first_name;
    this.last_name = last_name;
    this.date = date;
    this.available_time = available_time;
    this.email = email;
    this.phone = phone;
    this.bedroom_number = bedroom_number;
    this.bathroom_number = bathroom_number;
    this.company_name = company_name;
    this.building_type = building_type;
    this.number_rental = number_rental;
    this.average_cleaners = average_cleaners;
    this.message = message;
    this.extras = extras;
    this.total_amount = total_amount;
    this.location = location;
    this.card_number = card_number;
    this.card_holder = card_holder;
    this.exp_date = exp_date;
    this.CCV = CCV;
  }
}
