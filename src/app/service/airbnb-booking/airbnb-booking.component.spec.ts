import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AirbnbBookingComponent } from './airbnb-booking.component';

describe('AirbnbBookingComponent', () => {
  let component: AirbnbBookingComponent;
  let fixture: ComponentFixture<AirbnbBookingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AirbnbBookingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AirbnbBookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
