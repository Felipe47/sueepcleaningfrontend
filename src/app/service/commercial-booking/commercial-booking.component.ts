import { ServiceBookingModel } from './../commercial-info';
import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { MapsAPILoader, AgmMap } from '@agm/core';
import { NgForm, FormControl } from '@angular/forms';
import { BookingService } from 'src/app/shared/auth/booking.service';
import { FlatpickrOptions } from 'ng2-flatpickr';
import flatpickr from 'flatpickr';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-commercial-booking',
  templateUrl: './commercial-booking.component.html',
  styleUrls: ['./commercial-booking.component.scss']
})
export class CommercialBookingComponent implements OnInit {
  dateString: string;
  now: Date = new Date();
  minDate: any;
  public errorMessage: any;
  public exampleOptions: FlatpickrOptions = {
    defaultDate: new Date(),
    minDate: 'today',
    dateFormat: 'Y-m-d',
    onChange: (selectedDates, dateStr, instance) => {
      this.dateString = dateStr;
    },
    wrap: true,
    allowInput: false
  };
  address: any;
  private geoCoder;
  form: any = {};
  serviceBookingModel: ServiceBookingModel;
  public latitude: number;
  public longitude: number;
  public zoom: number;
  public latlongs: any = [];
  public latlong: any = {};
  public searchControl: FormControl;
  geocoder: any;
  @ViewChild('search', { static: false })
  public searchElementRef: ElementRef;
  @ViewChild('f', { static: false }) commercialBookingForm: NgForm;
  @ViewChild(AgmMap, { static: true }) map: AgmMap;

  constructor(private mapsAPILoader: MapsAPILoader,
    private router: Router,
    private ngZone: NgZone, private bookingService: BookingService) {
    this.mapsAPILoader = mapsAPILoader;
    this.mapsAPILoader.load().then(() => {
      this.geocoder = new google.maps.Geocoder();
    });
  }

  ngOnInit() {
    this.zoom = 3;
    this.latitude = 39.953021;
    this.longitude = -75.163479;
    this.searchControl = new FormControl();
    const dateVar = new Date(new Date().getTime() + 86400000);
    this.minDate = { year: dateVar.getFullYear(), month: dateVar.getMonth() + 1, day: dateVar.getDate() };
    this.form.date = this.minDate;
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: [],
        componentRestrictions: {'country' : 'USA'}
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          const latlong = {
            latitude: place.geometry.location.lat(),
            longitude: place.geometry.location.lng()
          };
          this.latlongs.push(latlong);
          // get the input value with the address
          this.address = place.name + '' + place.formatted_address;
          // this.searchControl.reset();
        });
      });
    });
    this.onSelect(this.minDate);
  }

  onSelect(event: any) {
    this.dateString = event.year + '-' + event.month + '-' + event.day;
  }


  sendQuote() {
    const url = 'api/user/create';
    const bookingInfoModel = {
      quote_type: 'Commercial',
      cleaning_type: '',
      date: this.dateString,
      available_time: this.form.available_time,
      first_name: this.form.first_name,
      last_name: this.form.last_name,
      email: this.form.email,
      phone: this.form.phone,
      bedroom_number: '',
      bathroom_number: '',
      company_name: this.form.company_name,
      building_type: this.form.building_type,
      number_rental: '',
      average_cleaners: '',
      additional_information: this.form.additional_information,
      address: this.address,
      extras: JSON.stringify(''),
      cardInfo: '',
      total_amount: ''
    };
    this.bookingService.create_quote(bookingInfoModel).subscribe(
      data => {
        // this.dato = JSON.parse(data.data);
        if (data.message[0].name) {
          Swal.fire(
            'Thanks for contacting us!',
            'Our team will be replying your message as soon as possible',
            'success'
          );
          this.router.navigate(['/home/welcome']);
        } else {
          Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',
            footer: 'chat with our support team for more information, thank you!'
          });
        }
      },
      error => {
        this.errorMessage = error;
        Swal.fire({
          type: 'error',
          title: 'Error...',
          text: 'Check your booking form please, if you think this is a mistake, chat with our support team, sorry for the inconvinients'
        });
      }
    );
  }

}
