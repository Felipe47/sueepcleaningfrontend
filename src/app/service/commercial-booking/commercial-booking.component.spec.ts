import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommercialBookingComponent } from './commercial-booking.component';

describe('CommercialBookingComponent', () => {
  let component: CommercialBookingComponent;
  let fixture: ComponentFixture<CommercialBookingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommercialBookingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommercialBookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
