// contact info model
export class ContactInfo {
  first_name: string;
  last_name: string;
  email: string;
  phone: string;
  message: string;

  constructor(first_name: string, last_name: string , email: string,
    phone: string, message: string) {
    this.first_name = first_name;
    this.last_name = last_name;
    this.email = email;
    this.phone = phone;
    this.message = message;
  }
}
