import { Component, OnInit, ViewChild } from '@angular/core';
import { ContactInfo } from '../contact-info';
import { ContactService } from 'src/app/shared/auth/contact.service';
import Swal from 'sweetalert2';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-contact-page',
  templateUrl: './contact-page.component.html',
  styleUrls: ['./contact-page.component.scss']
})
export class ContactPageComponent implements OnInit {
  errorMessage: string;
  // modelInfo: any;
  dato: any;
  contactInfo: ContactInfo;
  form: any = {};

  @ViewChild('f', { static: false }) contactForm: NgForm;
  constructor(private contactService: ContactService) { }

  ngOnInit() {
  }


  sendContactInfo() {
    // this.router.navigate(['/booking']);
    // const url = 'api/user/create';
    this.contactInfo = new ContactInfo(
      this.form.first_name,
      this.form.last_name,
      this.form.email,
      this.form.phone,
      this.form.additional_information,
    );
    this.contactService.connectionToServer(this.contactInfo).subscribe(
      data => {
        if (data.message.name) {
          Swal.fire(
            'Thanks for contacting us!',
            'Our team will be replying your message as soon as possible',
            'success'
          );
          this.contactForm.reset();
        } else {
          Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',
            footer: 'chat with our support team for more information, thank you!'
          });
        }
      },
    );
  }

}
