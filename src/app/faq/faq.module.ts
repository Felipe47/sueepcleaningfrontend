import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FAQRoutingModule } from './faq-routing.module';
import { MaterialModule } from '../material.module';
import { FaqComponentComponent } from './faq-component/faq-component.component';


@NgModule({
  declarations: [FaqComponentComponent],
  imports: [
    CommonModule,
    FAQRoutingModule,
    MaterialModule
  ]
})
export class FAQModule { }
