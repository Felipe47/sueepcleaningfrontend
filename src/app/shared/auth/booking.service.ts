import { BookingInfo } from './../../service/booking-info';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';


@Injectable({
  providedIn: 'root'
})


export class BookingService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Basic M2VlMzdlOGRhNGJiY2IwOjg5MWI4YmQ5ZWY0ZGMxMA==',
      'Accept': 'application/json'
    }),
  };

  private serverURL = environment.serverUrl;
  constructor(private http: HttpClient) { }


  // Servicios
  check_available_time(available_time_info): Observable<any> {
    return this.http.post<any>(this.serverURL + 'get_availability', available_time_info, this.httpOptions);
  }
  check_promo_code(promo_code_info): Observable<any> {
    return this.http.post<any>(this.serverURL + 'get_promo_code', promo_code_info, this.httpOptions);
  }

  create_quote(bookingInfo): Observable<any> {
    return this.http.post<any>(this.serverURL + 'create_quote', bookingInfo, this.httpOptions);
  }
}
