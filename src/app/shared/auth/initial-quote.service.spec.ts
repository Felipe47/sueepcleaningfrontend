import { TestBed } from '@angular/core/testing';

import { InitialQuoteService } from './initial-quote.service';

describe('InitialQuoteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InitialQuoteService = TestBed.get(InitialQuoteService);
    expect(service).toBeTruthy();
  });
});
