import { Observable } from 'rxjs';
import { HttpClientModule, HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Basic M2VlMzdlOGRhNGJiY2IwOjg5MWI4YmQ5ZWY0ZGMxMA=='
    }),
  };

  private serverURL = environment.serverUrl;

  constructor(private http: HttpClient) { }

  // Servicios
  connectionToServer(contactInfo): Observable<any> {
    return this.http.post<any>(this.serverURL + 'create_contact', contactInfo, this.httpOptions);
  }

}
