import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from './../../../environments/environment.prod';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InitialQuoteService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Basic M2VlMzdlOGRhNGJiY2IwOjg5MWI4YmQ5ZWY0ZGMxMA=='
    }),
  };

  private serverURL = environment.serverUrl;
  constructor(private http: HttpClient) { }


  // Servicios
  check_zone(zip_code):Observable<any>{
    return this.http.post<any>('https://dev-office.sueep.com/api/method/cleaning_services.cleaning_services.doctype.zip_codes_zone.zip_codes_zone.get_available_zip_code', zip_code, this.httpOptions);
  }
  initial_quote(customerInfo): Observable<any> {
    return this.http.post<any>(this.serverURL + 'initial_quote', customerInfo, this.httpOptions);
  }
}
