import { AgmCoreModule } from '@agm/core';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
// Funcionalidad para cambiar a pantalla completa mediante un botón.
// import { ToggleFullscreenDirective } from './directives/toggle-fullscreen.directive';
// Spinner inicial de "loading"
import { NgxSpinnerModule } from 'ngx-spinner';
import { FormsModule } from '@angular/forms';
import { Ng2FlatpickrModule } from 'ng2-flatpickr';


@NgModule({
  exports: [
    CommonModule,
    FooterComponent,
    NavbarComponent,
    // ToggleFullscreenDirective,
    NgbModule
  ],
  imports: [
    RouterModule,
    CommonModule,
    NgxSpinnerModule,
    NgbModule,
    FormsModule,
    Ng2FlatpickrModule,
    AgmCoreModule.forRoot({apiKey: 'AIzaSyAWRY8YMY19w8XKZcgTbkUDc6f58FOk_ew'})
  ],
  declarations: [
    FooterComponent,
    NavbarComponent
  ],
  // providers: [AuthGuard],
})
export class SharedModule { }
