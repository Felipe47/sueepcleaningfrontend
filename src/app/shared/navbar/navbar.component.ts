import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  nombre = '';
  public correoUsuario: String;
  public isCollapsed = true;
  placement = 'bottom-right';
  javascript: any;

  constructor() { }

  ngOnInit() {
    this.correoUsuario = 'Usuario';
    $.getScript('./assets/js/navbar.js');
  }

}
