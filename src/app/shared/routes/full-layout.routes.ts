import { Routes, RouterModule } from '@angular/router';

// Estas son las rutas que tendrá nuestra aplicación.

export const Full_ROUTES: Routes = [

  // homePage-module
  {
    path: '',
    loadChildren: './home/home.module#HomeModule'
  },
  // contactPage-module
  {
    path: 'contact',
    loadChildren: './contact/contact.module#ContactModule'
  },
  // servicePage-module
  {
    path: 'service',
    loadChildren: './service/service.module#ServiceModule'
  },
  // booking-module
  {
    path: 'cleaningservices',
    loadChildren: './booking/booking.module#BookingModule'
  },
  // download-module
  {
    path: 'download',
    loadChildren: './download/download.module#DownloadModule'
  },
   // faq-module
   {
    path: 'faq',
    loadChildren: './faq/faq.module#FAQModule'
  }
];
